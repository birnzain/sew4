﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _2CHIT_Übung10_Characters_Polymorphie_
{
    class Zombie:Hero
    {
        public Zombie()
        {
            name = "Zombie";
            hp = 10;
            attack = 1;
            defense = 0;
            speed = 0;
            level = 1;
            ability = "keine";
            Short = "Gegner";
            maxHp = hp;
        }
    }
}
