﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _2CHIT_Übung10_Characters_Polymorphie_
{
    class Tank : Hero
    {
        public Tank()
        {
            name = "Tank";
            hp = 50;
            attack = 2;
            defense = 3;
            speed = 1;
            level = 1;
            ability = "Extreme Verteidigung";
            maxHp = hp;
        }
        public Tank(string shor):this()
        {
            Short = shor;
        }




        public new float THp()
        {
            return hp;
        }

        public override void Dance()
        {
            Console.WriteLine("Tank is jumping around");
        }
    }
}
