﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _2CHIT_Übung10_Characters_Polymorphie_
{
    class Warrior : Hero
    {
        public Warrior()
        {
            name = "Warrior";
            hp = 30;
            attack = 6;
            defense = 2;
            speed = 2;
            level = 1;
            ability = "Extreme Stärke";
            maxHp = hp;
        }
        public Warrior(string shor):this()
        {
            Short = shor;
        }









        public override void Dance()
        {
            Console.WriteLine("Warrior is dancing and shouting");
        }
    }
}
