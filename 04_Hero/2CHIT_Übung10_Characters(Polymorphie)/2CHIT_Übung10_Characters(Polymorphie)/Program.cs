﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _2CHIT_Übung10_Characters_Polymorphie_
{
    class Program
    {
        static void Main(string[] args)
        {
            Tank t = new Tank("Gegner");
            Sourcerer s = new Sourcerer("Gegner");
            Warrior w = new Warrior("Gegner");
            Zombie z = new Zombie();
            Hero you;
            int decision;
            Random r = new Random();

            Console.WriteLine("Du wachst in einem dunklen Wald auf und stellst fest, dass du besondere Fähigkeiten hast.\n Wähle: 1. {0}\n\t2. {1}\n\t3. {2}", t.Ability, s.Ability, w.Ability );
            decision = Convert.ToInt32(Console.ReadLine());
            switch (decision)
            {
                case 1: you = new Tank("Du"); break;
                case 2: you = new Sourcerer("Du"); break;
                case 3: you = new Warrior("Du"); break;
                default:
                    you = null;
                    Environment.Exit(0);
                    break;
            }
            Console.WriteLine("Du hast dich für Option {0} entschieden. Du bist ein {1}.", decision, you.Name);
            switch (decision)
            {
                case 1: Console.WriteLine("Du hast eine unglaubliche Verteidigung, bist allerdings sehr schwerfällig und \nkannst nicht wirklich viel Schaden austeilen."); break;
                case 2: Console.WriteLine("Du bist sehr schnell und wendig und du hast die Fähigkeit Magie zu nutzen. Allerdings \nkannst du nicht besonders viel einstecken.");break;
                case 3: Console.WriteLine("Du kannst extrem viel Schaden austeilen. Allerdings bist du nicht gerade schnell \nund hast nicht die beste Panzerung."); break;
            }
            Console.WriteLine();
            Thread.Sleep(5000);

            Console.WriteLine("Was machst du als erstes?\n\t1. Du springst von einer Klippe da du damit nicht leben kannst.\n\t2. Du freust dich und fängst an zu tanzen");
            decision = Convert.ToInt32(Console.ReadLine());
            if (decision == 1)
            {
                Console.WriteLine("Du bist tot");
                Console.ReadKey();
                Environment.Exit(0);
            }

            else if (decision == 2)
                you.Dance();
            else
                Environment.Exit(0);
            Thread.Sleep(2000);

            Console.WriteLine("Plötzlich bemerkst du, dass eine unheimliche Kreatur auf dich zuläuft. \nWas machst du?\t1. Weglaufen\n\t\t2. Dich der Kreatur stellen und kämpfen \n\t\t3. Verstecken");
            decision = Convert.ToInt32(Console.ReadLine());
            switch (decision)
            {
                case 1:
                    Console.WriteLine("Du versuchst verzweifelt wergzulaufen, jedoch ist dein Gegner schneller als du und holt dich ein. \nAls du dich umdrehst um nach deinem Gegner zu schauen stolperst du über einen Baumstamm und fällst hin. \nDie Kreatur tötet dich!");
                    Console.ReadKey();
                    Environment.Exit(0);
                    break;
                case 2:
                    Console.WriteLine("Du stellst dich der Kreatur und ihr beginnt zu kämpfen. Es ist ein Zombie. Das wird keine leichte Aufgabe.");
                    Thread.Sleep(2000);
                    you.Fight(you, z, r.Next(1,2));
                    break;
                case 3:
                        Console.WriteLine("Du suchst dir schnell ein Versteck, damit dich die Kreatur nicht sieht. Du hast Glück, die Kreatur sieht dich nicht und läuft weiter.");
                    
                    break;
                default: Environment.Exit(0); break;
            }
            Console.WriteLine();
            Thread.Sleep(2000);
            Console.WriteLine("Du hast überlebt! \nWas jetzt?\n\t1. Ausrasten\n\t2. Sofort ins nächste Abenteuer laufen");
            decision = Convert.ToInt32(Console.ReadLine());
            switch (decision)
            {
                case 1: Console.WriteLine("Du entscheidest dich, nach diesem Anstrengenden Manöver, dich kurz auszurasten, um Energie zu tanken.");
                        you.Hp = you.MaxHp;
                        Thread.Sleep(1000);
                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        Console.WriteLine("Deine Energie wurde wieder hergestellt!");
                        Console.ResetColor();
                        break;
                case 2: Console.WriteLine("Ohne nachzudenken läufst du los, bemerst jedoch nicht, dass du direkt in eine Falle von\nRäubern läufst. Du läufst direkt in ein Fangnetz. Einige Stunden später\nkommen die Räuber und töten dich.");
                    Console.ReadKey();
                    Environment.Exit(0);
                        break;
                default: Environment.Exit(0); break;
            }
            Thread.Sleep(3000);
            Console.WriteLine();
            Console.WriteLine("Nach einer kurzen Pause beschließt du, vorsichtig den Wald zu erforschen.\nPlötzlich siehst du hiter einem Hügel eine Horde Zombies.");
            Console.WriteLine("Was machst du?\n\t1. Du fühlst dich unbesiegbar und stürmst auf die Zombies zu.\n\t2. Du beschließt die Zombies zu beobachten.\n\t3. Du verhältst dich ganz ruhig und gehst wieder zurück.");
            decision = Convert.ToInt32(Console.ReadLine());
            switch (decision)
            {
                case 1:
                    Console.WriteLine("Du fühlst dich zwar unbesiegbar. Leider bist du das aber nicht. Die Zombies töten dich!");
                    Console.ReadKey();
                    Environment.Exit(0);
                    break;
                case 2:
                    Console.WriteLine("Nach einigen Minuten dreht der Wind und die Zombies riechen dich. \nSofort setzt sich die Zombiehorde in Bewegung. Du hast keine Chance. Du bist tot!");
                    Console.ReadKey();
                    Environment.Exit(0);
                    break;
                case 3:
                    Console.WriteLine("Du hast Glück und die Zombies haben dich nicht bemerkt.");
                    break;
                default: Environment.Exit(0); break;
            }
            Thread.Sleep(1500);
            Console.WriteLine();
            Console.WriteLine("Du hast an Erfahrung gewonnen!");
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            you.LevelUp(you);
            Console.WriteLine("Neues Level: "+you.Level);
            Console.ResetColor();
            Thread.Sleep(1500);
            Console.WriteLine();

            Console.WriteLine("Auf dem Rückweg entdeckst du einen Busch mit saftigen Beeren. Du bist sehr hungrig.");
            Console.WriteLine("\t1. Essen\n\t2. Nicht essen");
            decision = Convert.ToInt32(Console.ReadLine());
            switch (decision)
            {
                case 1:
                    Console.WriteLine("Du isst die Beeren und der Hunger ist verschwunden");
                    Thread.Sleep(1500);
                    Console.ForegroundColor = ConsoleColor.DarkCyan;
                    you.LevelUp(you);
                    Console.WriteLine("Neues Level: " + you.Level);
                    Console.ResetColor();
                    Thread.Sleep(1500);
                    break;
                case 2:
                    Console.WriteLine("Du marschierst als so hungrige weiter.");
                    break;
                default: Environment.Exit(0); break;
            }
            Thread.Sleep(1500);
            Console.WriteLine();
            Console.WriteLine("Was jetzt?\n\t1. Einen Unterschlupf für die Nacht suchen\n\t2. Weiter den Wald erkunden");
            decision = Convert.ToInt32(Console.ReadLine());
            switch (decision)
            {
                case 1:
                    Console.WriteLine("Du eintscheidest dich, einen Unterschlupf für die Nacht zu suchen.");
                    break;
                case 2:
                    Console.WriteLine("Du erkundest weiter den Wald. Es wird immer dunkler und unheimlicher. Plötzlich wirst du von hinten überrascht. Du wirst getötet.");
                    Console.ReadKey();
                    Environment.Exit(0);
                    break;
                default: Environment.Exit(0); break;
            }
            Thread.Sleep(1500);
            Console.WriteLine("Wonach hältst du Ausschau?\n\t1. Ein Baumhaus\n\t2. Eine Höhle\n\t3. Du baust dir einen kleinen Unterschlupf unter einem Felsüberhang");
            decision = Convert.ToInt32(Console.ReadLine());
            switch (decision)
            {
                case 1:
                    Console.WriteLine("Du suchst und suchst. Leider findest du kein Baumhaus und musst im Freien übernachten. Im Schlaf wirst du von Zombies getötet.");
                    Console.ReadKey();
                    Environment.Exit(0);
                    break;
                case 2:
                    Console.WriteLine("Nach einiger Zeit findest du eine große Höhle und beschließt dort zu übernachten.\n Leider wird die Höhle von einem Ungeheur bewohnt. Du wirst getötet.");
                    Console.ReadKey();
                    Environment.Exit(0);
                    break;
                case 3:
                    Console.WriteLine("Schnell suchst du dir etwas Laub und ein paar Äste und verkriechst dich unter einem Felsüberhang.\nAm nächsten Morgen wachst du gut ausgeruht auf.");
                    break;
                default: Environment.Exit(0); break;
            }
            Thread.Sleep(1500);
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            you.LevelUp(you);
            Console.WriteLine("Neues Level: " + you.Level);
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Du beschließt, nach etwas Essbarem zu suchen.");
            Console.WriteLine("Plötzlich steht ein Tank vor dir. Ein Kampf ist unausweichlich.");
            you.Fight(you, t, r.Next(3, 4));
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            you.LevelUp(you);
            Console.WriteLine("Neues Level: " + you.Level);
            Console.ResetColor();
            Thread.Sleep(2000);
            Console.WriteLine();
            Console.WriteLine("Dein Kampf hat Aufsehen erregt. Ein Sourcerer reicht dir Beeren und fordert dich zum Kampf auf.");
            you.Hp = you.MaxHp;
            Thread.Sleep(1000);
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("Deine Energie wurde wieder hergestellt!");
            Console.ResetColor();
            Thread.Sleep(1000);
            Console.WriteLine("\t1. Kämpfen\n\t2. Nicht kämpfen und weggehen");
            decision = Convert.ToInt32(Console.ReadLine());
            switch (decision)
            {
                case 1:
                    Console.WriteLine("Du nimmst die Herausforderung an und ihr beginnt zu kämpfen.");
                    you.Fight(you, s, r.Next(3,5));
                    break;
                case 2:
                    Console.WriteLine("Du drehst dich um und gehst weg. Der Sourcerer tötet dich.");
                    Console.ReadKey();
                    Environment.Exit(0);
                    break;
                default: Environment.Exit(0); break;
            }
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            you.LevelUp(you);
            Console.WriteLine("Neues Level: " + you.Level);
            Console.ResetColor();
            Console.WriteLine();
            //////////////////////
            Console.WriteLine("Vielen Dank fürs Spielen der Testphase. Du hast dich gut geschlagen!");


            Console.ReadKey();
        }
    }
}
