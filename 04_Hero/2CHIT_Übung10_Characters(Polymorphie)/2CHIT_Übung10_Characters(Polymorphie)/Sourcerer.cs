﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _2CHIT_Übung10_Characters_Polymorphie_
{
   class Sourcerer : Hero
      {
        private int mana, maxMana;
        public Sourcerer()
        {
            name = "Sourcerer";
            hp = 20;
            attack = 4;
            defense = 1;
            speed = 3;
            mana = 1;
            maxMana = 1;
            level = 1;
            ability = "Magie";
            maxHp = hp;
        }
        public Sourcerer(string shor):this()
        {
            Short = shor;
        }
        public override void SetMana()
        {
            mana--;
        }
        public override int GetMana()
        {
            return mana;
        }
        public override void ReturnMana()
        {
            mana = maxMana;
        }
        public override void SetMaxMana(int value)
        {
            maxMana = value;
        }





        public override void Dance()
            {
                Console.WriteLine("Sourcerer is flying around");
            }
        }
    
}
