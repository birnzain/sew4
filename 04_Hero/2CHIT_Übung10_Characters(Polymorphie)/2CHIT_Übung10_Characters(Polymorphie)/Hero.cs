﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _2CHIT_Übung10_Characters_Polymorphie_
{
    
    class Hero
    {
        protected string name;
        protected float hp;
        protected int attack;
        protected int defense;
        protected int speed;
        protected int level;
        protected string ability;
        protected float maxHp;
        public string Short { get; set; }
        #region Properties
        public string Ability
            {
                get{ return ability; }
            }
        public string Name
        {
            get { return name; }
        }
        public float Hp
        {
            get { return hp; }
            set
            {
                if (value == maxHp)
                    hp = value;
            }
        }
        public float MaxHp
        {
            get { return maxHp; }
        }
        public int Level
        {
            get { return level; }
        }
        #endregion

        public virtual void Fight(Hero you, Hero enemie, int eLevel)
        {
            int decision;
            for (int i = 0; i <= eLevel; i++)
            {
                enemie.LevelUp(enemie);
            }
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Du: {0} \nGegner: {1}", you.hp, enemie.hp);
            Console.ResetColor();
            if (you.speed >= enemie.speed)
            {
                Console.WriteLine("Du bist dran!");
                while (you.hp > 0 && enemie.hp > 0)
                {
                    Console.WriteLine("Wähle deine Attacke!");
                    #region switch Angreifer
                    switch (you.name)
                    {
                        case "Tank":
                            if (level < 5)
                            {
                                Console.WriteLine("\t1. Tackle");
                                decision = Convert.ToInt32(Console.ReadLine());
                                enemie.hp = AttacksTank(decision, you, enemie);
                            }
                            else if (level < 10)
                            {
                                Console.WriteLine("\t1. Tackle\n\t2. Bodyslam");
                                decision = Convert.ToInt32(Console.ReadLine());
                                enemie.AttacksTank(decision, you, enemie);
                            }
                            else if (level >= 10)
                            {
                                Console.WriteLine("\t1. Tackle\n\t2. Bodyslam\n\t3. Erdbeben");
                                decision = Convert.ToInt32(Console.ReadLine());
                                enemie.AttacksTank(decision, you, enemie);
                            }
                            break;
                        case "Sourcerer":
                            if (level < 5)
                            {
                                Console.WriteLine("\t1. Tackle");
                                decision = Convert.ToInt32(Console.ReadLine());
                                enemie.hp = AttacksSourcerer(decision, you, enemie);
                            }
                            else if (level < 10)
                            {
                                Console.WriteLine("\t1. Tackle\n\t2. Finte");
                                decision = Convert.ToInt32(Console.ReadLine());
                                enemie.hp = AttacksSourcerer(decision, you, enemie);
                            }
                            else if (level >= 10)
                            {
                                Console.WriteLine("\t1. Tackle\n\t2. Finte\n\t3. Zauberball");
                                decision = Convert.ToInt32(Console.ReadLine());
                                enemie.hp = AttacksSourcerer(decision, you, enemie);
                            }
                            break;
                        case "Warrior":
                            if (level < 5)
                            {
                                Console.WriteLine("\t1. Tackle");
                                decision = Convert.ToInt32(Console.ReadLine());
                                enemie.hp = AttacksWarrior(decision, you, enemie);
                            }
                            else if (level < 10)
                            {
                                Console.WriteLine("\t1. Tackle\n\t2. Schwertschlag");
                                decision = Convert.ToInt32(Console.ReadLine());
                                enemie.hp = AttacksWarrior(decision, you, enemie);
                            }
                            else if (level >= 10)
                            {
                                Console.WriteLine("\t1. Tackle\n\t2. Schwertschlag\n\t3. Nahkampf");
                                decision = Convert.ToInt32(Console.ReadLine());
                                enemie.hp = AttacksWarrior(decision, you, enemie);
                            }
                            break;
                        
                    }
                    #endregion
                    #region switch Angreifer2 
                    switch (enemie.name)
                    {
                        case "Zombie":
                            you.hp = AttacksZombie(1, enemie, you);
                            break;
                        case "Tank":
                            if (level < 5)
                            {
                                you.hp = AttacksTank(1, enemie, you);
                            }
                            else if (level < 10)
                            {
                                you.hp = AttacksTank(2, enemie, you);
                            }
                            else if (level >= 10)
                            {
                                you.hp = AttacksTank(3, enemie, you);
                            }
                            break;
                        case "Warrior":
                            if (level < 5)
                            {
                                you.hp = AttacksWarrior(1, enemie, you);
                            }
                            else if (level < 10)
                            {
                                you.hp = AttacksWarrior(2, enemie, you);
                            }
                            else if (level >= 10)
                            {
                                you.hp = AttacksWarrior(3, enemie, you);
                            }
                            break;
                        case "Sourcerer":
                            if (level < 5)
                            {
                                enemie.hp = AttacksSourcerer(1, enemie, you);
                            }
                            else if (level < 10)
                            {
                                enemie.hp = AttacksSourcerer(2, enemie, you);
                            }
                            else if (level >= 10)
                            {
                                enemie.hp = AttacksSourcerer(3, you, enemie);
                            }
                            break;
                    }
                    #endregion
                    if (you.hp > 0 && enemie.hp > 0)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Du: {0} \nGegner: {1}", you.hp, enemie.hp);
                        Console.ResetColor();
                        Thread.Sleep(2000);
                    }
                    WinAnalysis(you, enemie);
                }
            }





            else
            {
                Console.WriteLine("Dein Gegner ist an der Reihe!");
                while (you.hp > 0 && enemie.hp > 0)
                {
                    #region switch Angreifer 
                    switch (enemie.name)
                    {
                        case "Zombie":
                            you.hp = AttacksZombie(1, enemie, you);
                            break;
                        case "Tank":
                            if (level < 5)
                            {
                                you.hp = AttacksTank(1, enemie, you);
                            }
                            else if (level < 10)
                            {
                                you.hp = AttacksTank(2, enemie, you);
                            }
                            else if (level >= 10)
                            {
                                you.hp = AttacksTank(3, enemie, you);
                            }
                            break;
                        case "Warrior":
                            if (level < 5)
                            {
                                you.hp = AttacksWarrior(1, enemie, you);
                            }
                            else if (level < 10)
                            {
                                you.hp = AttacksWarrior(2, enemie, you);
                            }
                            else if (level >= 10)
                            {
                                you.hp = AttacksWarrior(3, enemie, you);
                            }
                            break;
                        case "Sourcerer":
                            if (level < 5)
                            {
                                enemie.hp = AttacksSourcerer(1, enemie, you);
                            }
                            else if (level < 10)
                            {
                                enemie.hp = AttacksSourcerer(2, enemie, you);
                            }
                            else if (level >= 10)
                            {
                                enemie.hp = AttacksSourcerer(3, enemie, you);
                            }
                            break;
                    }
                    #endregion
                    #region switch Angreifer2 
                    Console.WriteLine("Wähle deine Attacke!");
                    switch (you.name)
                    {
                        case "Tank":
                            if (level < 5)
                            {
                                Console.WriteLine("\t1. Tackle");
                                decision = Convert.ToInt32(Console.ReadLine());
                                enemie.hp = AttacksTank(decision, you, enemie);
                            }
                            else if (level < 10)
                            {
                                Console.WriteLine("\t1. Tackle\n\t2. Bodyslam");
                                decision = Convert.ToInt32(Console.ReadLine());
                                enemie.hp = AttacksTank(decision, you, enemie);
                            }
                            else if (level >= 10)
                            {
                                Console.WriteLine("\t1. Tackle\n\t2. Bodyslam\n\t3. Erdbeben");
                                decision = Convert.ToInt32(Console.ReadLine());
                                enemie.hp = AttacksTank(decision, you, enemie);
                            }
                            break;
                        case "Sourcerer":
                            if (level < 5)
                            {
                                Console.WriteLine("\t1. Tackle");
                                decision = Convert.ToInt32(Console.ReadLine());
                                enemie.hp = AttacksSourcerer(decision, you, enemie);
                            }
                            else if (level < 10)
                            {
                                Console.WriteLine("\t1. Tackle\n\t2. Finte");
                                decision = Convert.ToInt32(Console.ReadLine());
                                enemie.hp = AttacksSourcerer(decision, you, enemie);
                            }
                            else if (level >= 10)
                            {
                                Console.WriteLine("\t1. Tackle\n\t2. Finte\n\t3. Zauberball");
                                decision = Convert.ToInt32(Console.ReadLine());
                                enemie.hp = AttacksSourcerer(decision, you, enemie);
                            }
                            break;
                        case "Warrior":
                            if (level < 5)
                            {
                                Console.WriteLine("\t1. Tackle");
                                decision = Convert.ToInt32(Console.ReadLine());
                                enemie.hp = AttacksWarrior(decision, you, enemie);
                            }
                            else if (level < 10)
                            {
                                Console.WriteLine("\t1. Tackle\n\t2. Schwertschlag");
                                decision = Convert.ToInt32(Console.ReadLine());
                                enemie.hp = AttacksWarrior(decision, you, enemie);
                            }
                            else if (level >= 10)
                            {
                                Console.WriteLine("\t1. Tackle\n\t2. Schwertschlag\n\t3. Nahkampf");
                                decision = Convert.ToInt32(Console.ReadLine());
                                enemie.hp = AttacksWarrior(decision, you, enemie);
                            }
                            break;
                    }
                    #endregion

                    if (you.hp > 0 && enemie.hp > 0)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Du: {0} \nGegner: {1}", you.hp, enemie.hp);
                        Console.ResetColor();
                        Thread.Sleep(2000);
                    }
                    WinAnalysis(you, enemie);



                }
            }
                

        
        }












        public float AttacksTank(int decision, Hero one, Hero two)
        {
            switch (decision)
            {
                case 1:
                        Console.WriteLine("{0} setzt Tackle ein.", one.Short);
                        Thread.Sleep(1500);
                        return Tackle(one, two);
                        
                    break;
                case 2:
                    Console.WriteLine("{0} setzt Bodyslam ein.", one.Short);
                    Thread.Sleep(1500);
                    return Bodyslam(one, two);
                    break;
                case 3:
                    Console.WriteLine("{0} setzt Erdbeben ein.", one.Short);
                    Thread.Sleep(1500);
                    return Earthquake(one, two);
                    break;
                default:;  break;
            }
            return 0;
        }
        public float AttacksWarrior(int decision, Hero one, Hero two)
        {
            switch (decision)
            {
                case 1:
                    Console.WriteLine("{0} setzt Tackle ein.", one.Short);
                    Thread.Sleep(1500);
                    return Tackle(one, two);

                    break;
                case 2:
                    Console.WriteLine("{0} setzt Schwertschlag ein.", one.Short);
                    Thread.Sleep(1500);
                    return SwordBlow(one, two);
                    break;
                case 3:
                    Console.WriteLine("{0} setzt Nahkampf ein.", one.Short);
                    Thread.Sleep(1500);
                    return CloseCombat(one, two);
                    break;
                default: break;
            }
            return 0;
        }
        public float AttacksSourcerer(int decision, Hero one, Hero two)
        {
            switch (decision)
            {
                case 1:
                    Console.WriteLine("{0} setzt Tackle ein.",one.Short);
                    Thread.Sleep(1500);
                    return Tackle(one, two);

                    break;
                case 2:
                    Console.WriteLine("{0} setzt Finte ein.", one.Short);
                    Thread.Sleep(1500);
                    return Feint(one, two);
                    break;
                case 3:
                    if (GetMana() > 0)
                    {
                    Console.WriteLine("{0} setzt Zauberball ein.", one.Short);
                    Thread.Sleep(1500);
                    return MagicBall(one, two);
                    }
                    else
                    {
                        Console.WriteLine("Du hast kein Mana mehr!");
                        goto case 2;
                    }
                    
                    break;
                default: break;
            } return 0;
        }
        public float AttacksZombie(int decision, Hero one, Hero two)
        {
            Console.WriteLine("{0} setzt Biss ein.", one.Short);
            Thread.Sleep(1500);
            return Bite(one, two);
        }









        #region Attacks
        public virtual float Tackle(Hero one, Hero two)
        {
            float damage;
            damage = one.attack *2* one.level - two.defense;
            return AttackReturner(two, damage);
        }
        public virtual float Bodyslam(Hero one, Hero two)
        {
            float damage;
            damage = one.attack*2 * one.level*1.2f - two.defense;
            return AttackReturner(two, damage);

        }
        public virtual float Earthquake(Hero one, Hero two)
        {
            float damage;
            damage = one.attack*2 * one.level*1.5f - two.defense;
            return AttackReturner(two, damage);
        }
        public virtual float Feint(Hero one, Hero two)
        {
            float damage;
            damage = one.attack * 2 * one.level * 1.2f - two.defense;
            return AttackReturner(two, damage);
        }
        public virtual float MagicBall(Hero one, Hero two)
        {
            float damage;
            SetMana();
            damage = one.attack * 2 * one.level * 1.2f - two.defense;
            return AttackReturner(two, damage);
        }
        public virtual float SwordBlow(Hero one, Hero two)
        {
            float damage;
            damage = one.attack *2* one.level*1.2f - two.defense;
            return AttackReturner(two, damage);
        }
        public virtual float CloseCombat(Hero one, Hero two)
        {
            float damage;
            damage = one.attack *2* one.level*1.2f - two.defense;
            return AttackReturner(two, damage);
        }
        public virtual float Bite(Hero one, Hero two)
        {
            float damage;
            damage = one.attack *2* one.level  - two.defense;
            return AttackReturner(two, damage);
        }
#endregion



        public virtual float AttackReturner(Hero two, float damage)
        {
                two.hp -= damage;
            if (two.hp > two.maxHp)
                return two.maxHp;
            else
                return two.hp;
        }

        public virtual void WinAnalysis(Hero you, Hero enemie)
        {
            
            if (enemie.hp <= 0 || (enemie.hp <= 0 && you.hp <= 0))
            {
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine("Du hast gewonnen!");
                Console.ResetColor();
            }
            if (you.hp <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Du hast verloren!");
                Console.ResetColor();
                Console.ReadKey();
                Environment.Exit(0);
            }
        }


        public virtual void LevelUp(Hero you)
        {
            if(you.name=="Zombie")
            {
                hp++;
                maxHp++;
                if (level % 5 == 0)
                {
                    attack ++;
                    defense++;
                    hp += 10;
                    maxHp += 10;
                }
            }
            else
            {
                level++;
                hp += 10;
                maxHp += 10;
                if (level % 3 == 0)
                {
                    attack++;
                    defense++;
                    hp += 10;
                    maxHp += 10;
                }
                if (you.name == "Sourcerer" && level == 10)
                {
                    SetMaxMana(3);
                    ReturnMana();
                }
            }
            

        }
        public virtual void SetMana() { }
        public virtual int GetMana() { return 0; }
        public virtual void ReturnMana() { }
        public virtual void SetMaxMana(int value) { }



        public virtual void Dance()
        {
            Console.WriteLine("Hero is dancing");
        }

    }
}
