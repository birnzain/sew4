//Programm gibt die ersten x-Zeichen einer Datei aus. Mit -t wird ein Tabulator davor gesetzt.
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <ctype.h>

//Wenn -n nicht angegeben ist beim Aufruf, wird 5 angegeben und 5 Zeichen pro Zeile ausgegeben.
#define DEFAULT_NUM 5

//Hier wird der Name des Programms gespeichert
char *szCommand = 0;

int processarg(int argc, char **argv, long *num, char *tab);
void BailOut(const char *szMessage);
void Usage(void);

int main(int argc, char **argv)
{

    long number=0;
    char tabulator='n',zeichen=' ';
    FILE *fp;
    int i=1;

    szCommand=argv[0];

	if (processarg(argc,argv,&number,&tabulator)==EXIT_FAILURE)
	{
		BailOut("Fehler bei der Auswertung der Argumente");
    }
    //Wenn kein Dateiname angegeben ist -> Angabe wie mans richtig aufruft
    if (optind>argc-1) Usage();

    //Wenn Dateienamen angegeben sind
    while (optind<argc){
		//�ffne Datei
		if ((fp=fopen(argv[optind],"r"))==NULL){
			BailOut("Fehler beim Oeffnen der Datei!");
		}
		//gibt (tabulator wenn gestzt) und die ersten n (number) Zeichen jeder Zeile aus.
		else
		{
			printf("-------------------- File:  %s ---------------------\n",argv[optind]);
			if (tabulator=='y'){
				printf("%c",'\t');
			}

			while((zeichen=fgetc(fp))!=EOF)
			{
				if ((zeichen!='\n')&&(i<=number))
				{
					printf("%c",zeichen);
					i++;
				}
				if (zeichen=='\n')
				{
					printf("%c",'\n');
				if (tabulator=='y')
				{
					printf("%c",'\t');
				}
				i=1;
				}
			}
		}
		if (fclose(fp)) BailOut("Fehler beim Schlie�en der Datei");
	
		optind++;
    }
    printf("\n");
    return EXIT_SUCCESS;
}

//arbeitet mit getopt die parameter auf
int processarg(int argc, char **argv, long *num, char *tab){

    int c=0, optionn=0, optiont=0;
    long number=DEFAULT_NUM;
    char *eptr=(char *)0;

    *tab='n';
    while((c=getopt(argc,argv,"n:t?"))!=EOF)"{
	switch(c){
            //gibt an wieviele zeichen ausgegeben werden sollen pro zeile von der Datei
	    case 'n':
		if (optionn) Usage();
		optionn=1;
		number=strtol(optarg,&eptr,10); //wandelt string in long um (setzt
						//errorpointer falls fehler auftritt
		//wenn errorpointer auf argument zeigt ist ein fehler aufgetreten
		if ((eptr==optarg)||(*eptr!='\0')) BailOut("Wrong Argument");
		break;
	    //gibt an ob vor der ausgabe ein Tabulatorzeichen stehen soll oder nicht
	    case 't':
		if (optiont) Usage();
		optiont=1;
		*tab='y';
		break;
	    //ung�ltiger aufruf - gibt an wie das programm
	    //zum aufrufen geht und gibt error aus.
	    case '?':
		Usage();
		break;
	    default:
		return EXIT_FAILURE;
		break;
	}
    }
    *num=number;
    return EXIT_SUCCESS;
}

//Wenn ein Pointer der auf Null zeigt �bergeben wird, steigt er einfach aus, wenn ein Pointer auf ein CharArray �bergeben wird gibt er dieses aus und steigt dann aus.
void BailOut(const char *szMessage){
    if (szMessage != (const char *) 0){
        (void) fprintf(stderr,"%s\n",szMessage);
    }
    exit(EXIT_FAILURE);
}

//Usage gibt an welche Parameter verwendet werden k�nnen. Wird ausgegeben wenn das Programm falsch aufgerufen wurde.
void Usage(void){
    (void) fprintf(stderr,"USAGE: %s [-n number] [-t] file\n",szCommand);
    BailOut((const char *) 0);
}
