#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <ctype.h>

/* globals */
char *szCommand="<not yet set>";

/* prototypes */
void BailOut(const char *szMessage);
void Usage();
void processarg(int argc, char *argv[], char *zeichen, int *caseinsensitive, char *dateiname[]);


/* main: counting characters in a file */
int main(int argc, char *argv[])
{

  char tosearch,c;
  int insensitive=0,count=0;
  FILE *fp=NULL;
  char *datei;

  processarg(argc,argv,&tosearch,&insensitive,&datei);

  printf("zeichen: %c\n",tosearch);
  printf("caseinsensitive: %d\n",insensitive);
  printf("datei: %s\n\n",datei);

  if ((fp=fopen(datei,"rt"))==NULL){
    BailOut("Fehler beim Oeffnen der Datei");
  }
  if (insensitive){
    tosearch=toupper(tosearch);
  }
  while ((c=fgetc(fp))!=EOF){
    if (insensitive){
      c=toupper(c);
    }
    if (c==tosearch) count++;
  }
  if (fclose(fp)) BailOut("Fehler beim Schlie�en");

  printf("Anzahl: %d\n",count);

  return EXIT_SUCCESS;
}



/* processarg: Verarbeitung der Argumente  */
void processarg(int argc, char *argv[], char *zeichen, int *caseinsensitive, char *dateiname[])
{
  int c,optionc=0,optioni=0;

  szCommand=argv[0]; /* programmname */
  *caseinsensitive=0;
  
  while ((c=getopt(argc,argv,"c:i"))!=EOF)
  {
      switch(c){
	  case 'c':
	      optionc++;
	      if (strlen(optarg)>1) Usage();
	      *zeichen=*optarg;  /* optarg zeigt auf das Argument
	                          zur aktuellen Option (=c) */
	      break;
	  case 'i':
	      optioni++;
	      *caseinsensitive=1;
	      break;
	  default:             /* ungueltige option */
	      Usage();        
	      break;
      }
  }

  if (optionc!=1) Usage(); /* option c genau 1x vorhanden */
  if (optioni>1) Usage();  /* option 1 hoechstens 1x vorhanden */

  if (optind!=argc-1) Usage(); /* genau 1 Argument nach Optionen? */
  *dateiname=argv[optind]; /* Dateiname=1.Argument nach Optionen */

}

/* BailOut: Ausgabe einer Fehlermeldung, Beenden des Programmes */
void BailOut(const char *szMessage){
 if (szMessage != (const char *) 0){
   (void) fprintf(stderr,"%s\n",szMessage);
 }
 exit(EXIT_FAILURE);
}

/* Usage: Meldung wie das Programm zu verwenden ist  */
void Usage(){
 (void) fprintf(stderr,"USAGE: %s -c char [-i] file\n",szCommand);
 BailOut((const char *) 0);
}
